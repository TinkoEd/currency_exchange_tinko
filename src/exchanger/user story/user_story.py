from src.exchanger.controller.controller import Controller


def main(controller):
    while True:
        controller.print_start()
        command_user = input()
        command, list_command = controller.get_command(command_user)
        match command:
            case 'STOP':
                print('SERVICE STOPPED')
                break
            case 'COURSE':
                controller.get_course(list_command)
            case 'EXCHANGE':
                controller.get_exchange(list_command)


if __name__ == '__main__':
    main(Controller())
