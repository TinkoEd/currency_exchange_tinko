class Course:

    def __init__(self, rate, available):

        self.rate = rate
        self.available = available

    def __str__(self):
        return f'RATE {round(float(self.rate), 2)}, AVAILABLE {round(float(self.available), 2)}'
