class Exchange:

    def __init__(self, currency, amount, rate):
        self.currency = currency
        self.amount = amount
        self.rate = rate

    def __str__(self):
        return f'{str(self.currency).upper()} {round(self.amount, 4)}, RATE {round(self.rate, 6)}'
