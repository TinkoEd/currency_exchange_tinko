class MyExceptionCurrency(Exception):

    def __init__(self, currency):
        self.currency = currency

    def no_currency(self):
        print(f'INVALID CURRENCY {self.currency}')


class MyExceptionExchange(Exception):
    def __init__(self, currency, required_balance, available):
        self.currency = currency
        self.required_balance = required_balance
        self.available = available

    def no_enough_money(self):
        print(f'UNAVAILABLE, REQUIRED BALANCE {self.currency} {round(float(self.required_balance), 2)}, '
              f'AVAILABLE {round(float(self.available), 2)}')
