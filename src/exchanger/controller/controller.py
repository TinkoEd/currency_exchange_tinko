from src.exchanger.model.course import Course
from src.exchanger.model.exchange import Exchange
from src.exchanger.model.myexception import MyExceptionCurrency, MyExceptionExchange


import json
import requests
import csv
import os


class Controller:

    COURSE_URL = 'https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5'
    COURSE_USD = '../model/date/course_usd.csv'
    COURSE_UAH = '../model/date/course_uah.csv'

    def __init__(self):
        self.course_usd = self.read_course_from_file(Controller.COURSE_USD)
        self.course_uah = self.read_course_from_file(Controller.COURSE_UAH)

    def print_(self):
        return f'{self.course_usd[0]} {self.course_usd[1]}'

    @staticmethod
    def print_start():
        print('COMMAND?')

    @staticmethod
    def get_course_from_api(url):
        get_requests = requests.get(url)
        if get_requests.status_code == 200:
            res = json.loads(get_requests.content)
            return res
        return None

    def write_course_in_file(self, course_file, available):
        with open(course_file, 'w', newline='') as file:
            writer = csv.writer(file)
            course_usd = self.get_course_from_api(Controller.COURSE_URL)
            for item in course_usd:
                if item['ccy'] == "USD":
                    if item['ccy'] in str(course_file).upper():
                        writer.writerow([item['ccy'], item['sale'], available])
                    if item['base_ccy'] in str(course_file).upper():
                        writer.writerow([item['base_ccy'], item['buy'], available])

    def read_course_from_file(self, course_file):
        if not os.path.exists(course_file):
            available = self.available_for_read_file(course_file)
            self.write_course_in_file(course_file, available)
        with open(course_file, newline='') as file:
            reader = csv.reader(file)
            for item in reader:
                return item

    @staticmethod
    def available_for_read_file(course_file):
        available = 0
        if 'USD' in str(course_file).upper():
            available = 3000
        if 'UAH' in str(course_file).upper():
            available = 100000
        return available

    @staticmethod
    def get_command(command_user):
        list_command = str.split(command_user)
        command = list_command[0]
        return command, list_command

    @staticmethod
    def get_item_from_command(list_command):
        count = len(list_command)
        if count == 2 and list_command[0] == 'COURSE':
            currency = list_command[1]
            return currency
        elif count == 3 and list_command[0] == 'EXCHANGE':
            currency = list_command[1]
            amount = list_command[2]
            return currency, amount
        else:
            print('THE COMMAND IS NOT CORRECT')

    def get_course(self, list_command):
        currency = self.get_item_from_command(list_command)
        try:
            if currency == 'USD':
                course = Course(self.course_usd[1], self.course_usd[2])
                print(course)
            elif currency == 'UAH':
                course = Course(self.course_uah[1], self.course_uah[2])
                print(course)
            else:
                raise MyExceptionCurrency(currency)
        except MyExceptionCurrency as e:
            e.no_currency()

    def get_exchange(self, list_command):
        currency_user, amount_user = self.get_item_from_command(list_command)
        currency = rate = amount = amount_uah = amount_usd = None
        try:
            if currency_user == 'USD':
                currency, rate, amount, amount_uah, amount_usd = self.exchange_usd(amount_user)
                if amount_uah < 0:
                    raise MyExceptionExchange(currency, amount, float(self.course_uah[2]))
            if currency_user == 'UAH':
                currency, rate, amount, amount_uah, amount_usd = self.exchange_uah(amount_user)
                if amount_usd < 0:
                    raise MyExceptionExchange(currency, amount, float(self.course_uah[2]))
            print(Exchange(currency, amount, rate))
            self.write_course_in_file(Controller.COURSE_USD, amount_usd)
            self.write_course_in_file(Controller.COURSE_UAH, amount_uah)
        except MyExceptionExchange as e:
            e.no_enough_money()

    def exchange_usd(self, amount_user):
        currency = self.course_uah[0]
        rate = float(self.course_uah[1])
        amount = float(amount_user) * rate
        amount_uah = float(self.course_uah[2]) - amount
        amount_usd = float(self.course_usd[2]) + float(amount_user)
        return currency, rate, amount, amount_uah, amount_usd

    def exchange_uah(self, amount_user):
        currency = self.course_usd[0]
        rate = 1 / float(self.course_usd[1])
        amount = float(amount_user) * rate
        amount_usd = float(self.course_usd[2]) - amount
        amount_uah = float(self.course_uah[2]) + float(amount_user)
        return currency, rate, amount, amount_uah, amount_usd






